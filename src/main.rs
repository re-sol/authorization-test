use rand::Rng;
use reqwest::header::{self, ACCEPT_CHARSET, CACHE_CONTROL, CONNECTION, REFERER, USER_AGENT};

use crate::{config::Config, referers::referers_list, user_agent::user_agent_list};

mod config;
mod referers;
mod user_agent;

const DEBUG: bool = false;

fn main() {
    let mut rng = rand::thread_rng();

    // Config passing
    let config = Config::new();
    let username = config.key.get("username").unwrap();
    let password = config.key.get("password").unwrap();
    let url = config.key.get("url").unwrap();

    let tor_proxy = format!("socks5h://127.0.0.1:9050");

    if DEBUG {
        println!("Creating client.")
    }

    // Create header map
    let mut default_headers = header::HeaderMap::new();

    // Insert headers into header map
    default_headers.insert(CONNECTION, header::HeaderValue::from_static("keep-alive"));
    default_headers.insert(CACHE_CONTROL, header::HeaderValue::from_static("no-cache"));
    default_headers.insert(
        ACCEPT_CHARSET,
        header::HeaderValue::from_static("ISO-8859-1,utf-8;q=0.7,*;q=0.7"),
    );

    // Create a client
    let client = reqwest::blocking::Client::builder()
        .proxy(reqwest::Proxy::all(tor_proxy).unwrap())
        .default_headers(default_headers)
        .build()
        .expect("Failed to build the client");

    // User agent randomization
    let user_agent_list = user_agent_list();
    let random_user_agent_index = rng.gen_range(0..user_agent_list.len());

    // Referers randomization
    let referers_list = referers_list();
    let random_referer_index = rng.gen_range(0..referers_list.len());

    if DEBUG {
        println!(
            "Requesting {}..",
            format!("{url}/SOLWebApi/api/v1/AuthorizationStatus")
        );
    }

    // Get a status request
    let data = client
        .get(format!("{url}/SOLWebApi/api/v1/AuthorizationStatus"))
        .basic_auth(username, Some(password))
        .header(USER_AGENT, &user_agent_list[random_user_agent_index])
        .header(REFERER, &referers_list[random_referer_index])
        .send()
        .expect("Failed to get a status request")
        .text();

    if DEBUG {
        println!("Request done.")
    }

    // Print debug information
    println!("{}", data.as_ref().unwrap()); // Print JSON
    println!("User agent: {}", user_agent_list[random_user_agent_index]); // Print user agent

    // Parse JSON
    let json: serde_json::Value =
        serde_json::from_str(&data.unwrap()).expect("JSON is not formated well");
    if serde_json::from_value(
        json.get("Data")
            .expect("Could not read Data key from JSON")
            .to_owned(),
    )
    .unwrap()
    {
        println!("Successfully authenticated!")
    } else {
        println!("Failed to authenticate.")
    }
}
