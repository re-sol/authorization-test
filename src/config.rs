use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub key: HashMap<String, String>,
}

impl Config {
    pub fn new() -> Self {
        toml::from_str(&fs::read_to_string("config.toml").expect("Failed to read the config file"))
            .unwrap()
    }
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}
