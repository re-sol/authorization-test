#!/usr/bin/env bash

cp config.toml.example config.toml

printf "Username: "
read username
printf "Password: "
read password
printf "Skola Online URL (ex: https://skola.plzen-edu.cz): "
read url

sed "s/example_username/$username/g" -i config.toml
sed "s/example_password/$password/g" -i config.toml
sed "s/example_url/${url/\/\//\\/\\/}/g" -i config.toml

echo "Created config file successfully."
